#!/usr/bin/env bash
set -e

declare -a paths
declare -a tfvars_files

index=0

echo "\$@  :: $@"

for file_with_path in "$@"; do
  echo "file_with_path (pre replaced space)  ${file_with_path}"
  file_with_path="${file_with_path// /__REPLACED__SPACE__}"
  echo "file_with_path (post replaced space)  ${file_with_path}"

  fwpd=$(dirname "$file_with_path")
  echo "file_with_path (dirname) $fwpd"

  paths[index]=$(dirname "$file_with_path")

  if [[ "$file_with_path" == *".tfvars" ]]; then
    tfvars_files+=("$file_with_path")
  fi

  let "index+=1"
done

for path_uniq in $(echo "${paths[*]}" | tr ' ' '\n' | sort -u); do
  echo "path_uniq   ${path_uniq}"
  path_uniq="${path_uniq//__REPLACED__SPACE__/ }"

  pushd "$path_uniq" > /dev/null
  terraform fmt
  popd > /dev/null
done

# terraform.tfvars are excluded by `terraform fmt`
for tfvars_file in "${tfvars_files[@]}"; do
  tfvars_file="${tfvars_file//__REPLACED__SPACE__/ }"

  terraform fmt "$tfvars_file"
done
